function checkInputValue(event) {
  let inputValue = event.target.value;
  inputValue = inputValue.trim();

  if (inputValue.length > 1) {
    return {
      okay: true,
      value: inputValue,
    };
  } else {
    return {
      okay: false,
      value: undefined,
    };
  }
}

function addItem(event) {
  if (event.key == "Enter") {
    let result = checkInputValue(event);

    if (result.okay) {
      let inputValue = result.value;
      let listContainer = document.querySelector(".todo-items");
      let listItem = document.createElement("li");

      listItem.innerHTML = `<li class="todo-item incomplete border border-t-gray-400 flex w-[100%] justify-between items-center px-2">
      <button
        id="check-uncheck-btn"
        class="bg-white text-white border-solid border border-gray-400 rounded-full w-[30px] h-[30px] outline-none"
      >
        &#x2713;
      </button>
      <p
        id="todo-item-text"
        class="bg-white text-black w-[90%] px-4 py-4"
      >
        ${inputValue}
      </p>
      <input
        id="item-input"
        type="text"
        style="display: none"
        class="bg-white w-[90%] px-4 py-4 outline-none"
      />
      <button
        id="delete-btn"
        class="bg-white text-gray-400 border-solid border-1 border-black rounded-full w-[40px] h-[40px] hover:text-red-700 outline-none"
      >
      &#x2717;
      </button>
    </li>`;

      listContainer.appendChild(listItem);
      event.target.value = "";

      changeItemsLeft();
      handleEventListeners();
    }
  }
}

function removeClass(className) {
  let editElements = document.getElementsByClassName(className);
  if (editElements.length) {
    let element = editElements[0];
    element.classList.remove(className);
    element.getElementsByTagName("input")[0].style.display = "none";
    element.getElementsByTagName("p")[0].style.display = "unset";
  }
}

function editItem(event) {
  removeClass("edit");

  let paragraph = event.target;
  let listItem = paragraph.parentNode;
  let itemInput = paragraph.nextElementSibling;
  let paragraphText = paragraph.innerText;

  listItem.classList.add("edit");
  itemInput.style.display = "unset";
  paragraph.style.display = "none";
  itemInput.value = paragraphText;
  itemInput.focus();

  itemInput.addEventListener("change", (event) => {
    let result = checkInputValue(event);

    if (result.okay) {
      paragraph.innerText = result.value;
    } else {
      paragraph.innerText = paragraphText;
    }
    itemInput.style.display = "none";
    paragraph.style.display = "unset";
  });

  itemInput.addEventListener("blur", (event) => {
    paragraph.style.display = "unset";
    itemInput.style.display = "none";
  });
}

function deleteItem(event) {
  let listItem = event.target.parentNode;
  listItem.remove();
  changeItemsLeft();
  handleEventListeners();
}

function updateListContainer() {
  let listContainer = document.querySelector(".todo-items");
  let isAll = listContainer.classList.contains("all");
  let isCompleted = listContainer.classList.contains("completed");
  let isIncompleted = listContainer.classList.contains("incompleted");

  if (isIncompleted) {
    getAllIncompletedItems();
  } else if (isCompleted) {
    getAllCompletedItems();
  } else if (isAll) {
    getAllItems();
  }
}

function checkUncheckItem(event) {
  let listItem = event.target.parentNode;
  updateListItem(listItem);
}

function updateListItem(listItem) {
  let checkUncheckButton = listItem.querySelector("#check-uncheck-btn");
  let paragraph = listItem.querySelector("#todo-item-text");

  listItem.classList.toggle("incomplete");
  if (!listItem.classList.contains("incomplete")) {
    paragraph.style.textDecoration = "line-through";
    checkUncheckButton.classList.remove("text-white");
    checkUncheckButton.classList.add("text-green-600");
  } else {
    checkUncheckButton.classList.remove("text-green-600");
    checkUncheckButton.classList.add("text-white");
    paragraph.style.textDecoration = "none";
  }

  changeItemsLeft();
  updateListContainer();
}

function getAllIncompletedItems(event) {
  let listContainer = document.querySelector(".todo-items");
  let listItems = document.querySelectorAll(".todo-item");

  listContainer.classList.remove("all");
  listContainer.classList.remove("completed");
  listContainer.classList.add("incompleted");

  listItems.forEach((listItem) => {
    if (!listItem.classList.contains("incomplete")) {
      listItem.style.display = "none";
    } else {
      listItem.style.display = "flex";
    }
  });
}

function getAllCompletedItems(event) {
  let listContainer = document.querySelector(".todo-items");
  let listItems = document.querySelectorAll(".todo-item");

  listContainer.classList.remove("all");
  listContainer.classList.remove("incompleted");
  listContainer.classList.add("completed");

  listItems.forEach((listItem) => {
    if (listItem.classList.contains("incomplete")) {
      listItem.style.display = "none";
    } else {
      listItem.style.display = "flex";
    }
  });
}

function getAllItems(event) {
  let listContainer = document.querySelector(".todo-items");
  let listItems = document.querySelectorAll(".todo-item");

  listContainer.classList.remove("incompleted");
  listContainer.classList.remove("completed");
  listContainer.classList.add("all");

  listItems.forEach((listItem) => {
    listItem.style.display = "flex";
  });
}

function changeItemsLeft() {
  let itemsLeftParagraph = document.querySelector("#items-left-text");
  let incompletedItems = document.querySelectorAll(".incomplete");

  itemsLeftParagraph.textContent = `${incompletedItems.length} ${
    incompletedItems.length == 1 ? "item" : "items"
  } left!`;
}

function clearCompletedItems() {
  let listItems = document.querySelectorAll(".todo-item");

  listItems.forEach((listItem) => {
    if (!listItem.classList.contains("incomplete")) {
      listItem.remove();
    }
  });

  changeItemsLeft();
  handleEventListeners();
}

function updateAllItems(event) {
  let listItems = document.querySelectorAll(".todo-item");
  let incompletedItems = document.querySelectorAll(".incomplete");

  if (incompletedItems.length) {
    listItems.forEach((listItem) => {
      if (listItem.classList.contains("incomplete")) {
        updateListItem(listItem);
      }
    });
  } else {
    listItems.forEach((listItem) => {
      updateListItem(listItem);
    });
  }
}

function handleEventListeners() {
  let todoInput = document.getElementById("todo-text-input");
  let todoItems = document.querySelectorAll(".todo-item");
  let deleteButtons = document.querySelectorAll("#delete-btn");
  let checkUncheckBtns = document.querySelectorAll("#check-uncheck-btn");
  let activeBtn = document.querySelector("#active-btn");
  let completedBtn = document.querySelector("#completed-btn");
  let allBtn = document.querySelector("#all-btn");
  let clearCompletedBtn = document.querySelector("#clear-completed-btn");
  let todoFooter = document.querySelector(".todo-footer");
  let checkUncheckAllBtn = document.querySelector("#check-uncheck-all-btn");

  if (todoItems.length == 0) {
    todoFooter.style.display = "none";
    checkUncheckAllBtn.classList.add("text-white");
  } else {
    todoFooter.style.display = "flex";
    checkUncheckAllBtn.classList.add("text-gray-400");
  }

  todoInput.addEventListener("keypress", addItem);

  todoInput.addEventListener("click", (event) => {
    removeClass("edit");
  });

  todoItems.forEach((todoItem) => {
    let paragraph = todoItem.querySelector("#todo-item-text");
    paragraph.addEventListener("dblclick", editItem);
  });

  deleteButtons.forEach((deleteButton) => {
    deleteButton.addEventListener("click", deleteItem);
  });

  checkUncheckBtns.forEach((checkUncheckBtn) => {
    checkUncheckBtn.addEventListener("click", checkUncheckItem);
  });

  activeBtn.addEventListener("click", getAllIncompletedItems);
  completedBtn.addEventListener("click", getAllCompletedItems);
  allBtn.addEventListener("click", getAllItems);

  clearCompletedBtn.addEventListener("click", clearCompletedItems);

  checkUncheckAllBtn.addEventListener("click", updateAllItems);
}

handleEventListeners();
